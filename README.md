# Soal Shift Modul 1
## Sistem Operasi 2023

Kelompok IT02 :
| Nama | NRP |
| ---------------------- | ---------- |
| Azzahra Sekar Rahmadina | 5027221035 |
| Jacinta Syilloam | 5027221036 |
| Stephanie Hebrina Mabunbun Simatupang | 5027221069 |

## List of Contents
- [Soal 1](#soal-1) 
- [Soal 2](#soal-2)
- [Soal 3](#soal-3)
- [Soal 4](#soal-4)


## Soal 1
Pada nomor ini, kita diminta untuk mendownload top_lagu_milik_farrel.csv tetapi dengan nama playlist.csv. Kemudian kita diminta untuk mencari Top 5 Lagu Hip Hop, 5 Lagu paling rendah John Mayer, 10 lagu pada tahun 2004, dan lagu yang diciptakan Ibu Sri.

### Download tautan
Cara download tautan yang disediakan di soal:
```
wget -O playlist.csv --no-check-certificate -r 'https://drive.google.com/uc?export=download&id=1JYkupCAZTmwPZSZRsB61cklPM-LUjRHp'
```
| Flag | Description |
| ---------------------- | ---------- |
| -O | writes document to file |
| --no-check-certificate | don't validate the server's certificate |
| -r | recursive |
Berfungsi untuk melakukan download dari link dengan output playlist.csv

### Playlist_keren.sh
```
echo "=== Top 5 Lagu dengan Genre Hip Hop: ==="
grep -i "Hip Hop" playlist.csv | sort -t, -k15,15nr | head -n 5

echo "=== 5 lagu paling rendah ciptaan John Mayer: ==="
grep -i "John Mayer" playlist.csv | sort -t, -k15,15n | head -n 5

echo "=== Top 10 Lagu pada tahun 2004: ==="
grep -i ",2004," playlist.csv | sort -t, -k15,15nr | head -n 10

echo "=== Lagu yang diciptakan oleh Ibu Sri: ==="
grep -i "Sri" playlist.csv 
```

#### Penjelasan
1. echo digunakan untuk print atau menampilakn kata-kata yang diapat tanda “ yaitu Top 5 Lagu dengan Genre Hip Hop. grep digunakan untuk mencari kata, -i adalah opsi yang membuat pencarian bersifat case-insensitive (mengabaikan perbedaan huruf besar dan kecil).
2. Sort digunakan untuk mengurutkan baris dalam file teks, -t, menentukan bahwa pemisah yang memisahkan kolom adalah koma. -k15 digunakan untuk mengurutkan berdasarkan kolom ke-15, nr digunakan untuk mengurutkan dari atas (popularity paling tinggi ke rendah)
3. Head digunakan untuk menampilkan beberapa baris pertama dari file, -n (angka) untuk mengambil berapa banyak baris yang mau ditampilkan. 

#### Revisi
```
grep -i "Hip Hop" playlist.csv | sort -t, -k15,15nr | head -n 5 | awk -F ',' '{print $2, $4, $15}' 
```
Revisi disini adalah menambah awk -F ',' '{print $2, $4, $15}'untuk print/menampilkan Judul lagu, genre lagu, dan popularity dari kolom 2,4, dan 15.

### Output
![Screenshot_2023-09-29_093848](/uploads/3ca81ddb1b49530e044dd1e16ac0b2c8/Screenshot_2023-09-29_093848.png)


## Soal 2
Pada soal nomor 2 tersebut, praktikan diminta untuk membuat dua skrip: "register.sh" untuk membuat sistem register dan "login.sh" untuk sistem login. Semua informasi pengguna, termasuk email, username, dan password, akan disimpan dalam file "users.txt". Kriteria yang harus dipenuhi untuk password telah tertera dalam soal, dan praktikan juga harus mencatat setiap percobaan login dan registrasi dalam file "auth.log" sesuai dengan format yang telah ditentukan.
Untuk menjalankan tugas ini, berikut adalah langkah-langkah yang kami lakukan:

### Sistem Register
Untuk membuat sistem register sesuai ketentuan soal, maka perlu untuk membuat file users.txt untuk menyimpan data register dan log.txt yang digunakan untuk menyimpan riwayat aktivitas yang terjadi dalam directory yang akan digunakan terlebih dahulu. 
Untuk membuat program register.sh, dapat membuat bash dengan mengetikkan perintah berikut pada terminal .
```nano register.sh```

### register.sh
```
#!/bin/bash


filename="users.txt"
auth_file="auth.log"
note_log() {
        local type="$1"
        local messages="$2"
        local timestamp=$(date "+[%d/%m/%y %H:%M:%S]")
        echo "$timestamp [$type] $message" >> "$auth_file"
        }

```

Terdapat deklarasi variabel yang meliputi filename mendeklarasikan variable users.txt yang berisi nama file untuk menyimpan informasi pengguna, dan juga ada auth_file mendeklarasikan auth.log yang berisi nama file log yang di autentikasi.

```
read -p "Enter your Email: " email

    #Cek email
        if grep -q "^$email" "$filename"; then
                echo "REGISTER FAILED - Email $email is already registered."
                note_log "REGISTER FAILED" "Email $email is already registered."
                exit 1
        fi
read -p "Enter your Username: " username
```

Pengguna diminta untuk menginput alamat email dengan menggunakan perintah read. Prompt "Enter your Email:" akan ditampilkan, dan input yang dimasukkan akan disimpan dalam variabel email. Setelah alamat email di input, program akan memeriksa sudah digunakan atau belum di file "users.txt" dengan menggunakan perintah grep. Jika alamat email itu telah terdaftar maka registrasi akan dianggap gagal, pesan kesalahan akan muncul pada layar, dan akan mencatat pesan gagal ke dalam file "auth.log", lalu program akan keluar dengan status kode 1. Sebaliknya, apabila validasi emailnya aman, maka program akan lanjut berjalan.

```
    read -s -p "Enter your Password: " password
    echo

    # Cek password
    if [ ${#password} -lt 8 ] || ! [[ "$password" =~ [A-Z] ]] || ! [[ "$password" =~ [a-z] ]] || \
        ! [[ "$password" =~ [0-9] ]] || [[ "$password" == *"$username"* ]]; then
            echo "REGISTER FAILED - Password does not meet criteria."
            note_log "REGISTER FAILED" "Password does not meet criteria for email $email."
            exit 1
    fi

    # Cek apakah password memiliki setidaknya 1 simbol unik
    unique_symbols="!@#\$%^&*()_+{}\[\]:;<>,.?|\\"
    has_unique_symbols=false

    for ((i = 0; i < ${#unique_symbols}; i++)); do
        char="${unique_symbols:$i:1}"
    if [[ "$password" == *"$char"* ]]; then
            has_unique_symbols=true
        break
    fi
    done

    if ! $has_unique_symbols; then
        echo "REGISTER FAILED - Password must contain at least 1 special character."
        note_log "REGISTER FAILED" "Password does not contain at least 1 special character."
        exit 1
    fi
```

program akan meminta pengguna untuk memasukkan kata sandi baru. pada proses read password ditambah -s agar password yang diinput pengguna tidak tampil. Lalu input yang dimasukkan akan disimpan dalam variabel password. Password yang dimasukkan oleh pengguna akan divalidasi oleh serangkaian perintah if.  Jika password yang dimasukkan tidak memenuhi persyaratan, program akan mencetak pesan kesalahan dan program akan keluar dengan status kode 1. Jika password yang dimasukkan memenuhi semua persyaratan, program akan menerima password dan lanjut ke bagian selanjutnya. 

### Syarat
1. Untuk syarat pertama, yaitu Password yang dibuat harus lebih dari 8 karakter menggunakan operator ${#password} untuk mengukur panjang karakter dalam variabel password. Selanjutnya, kami menggunakan operator -lt untuk membandingkan panjang karakter password dengan angka 8. 
2. Pada syarat kedua, yaitu Harus terdapat paling sedikit 1 huruf kapital dan 1 huruf kecil, 
"$password" =~ [A-Z] akan mengevaluasi apakah password mengandung setidaknya 1 huruf kapital. Pada bagian kedua, "$password" =~ [a-z] akan mengevaluasi apakah password mengandung setidaknya 1 huruf kecil. 
3. Pada syarat ketiga, yaitu Harus terdapat paling sedikit 1 angka, "$password" =~ [0-9] akan mengevaluasi apakah password mengandung setidaknya 1 angka.
4. Untuk syarat keempat, yaitu Password tidak boleh sama dengan username, digunakan "$password" == *"$username"* untuk membandingkan nilai dari variabel password dengan nilai dari variabel username.
5. Untuk syarat kelima, yaitu Harus terdapat paling sedikit 1 simbol unik, digunakan for loop untuk memeriksa karakter-karakter simbol khusus satu per satu dalam password. Jika ditemukan setidaknya satu karakter simbol khusus, variabel has_unique_symbols diubah menjadi true.

### Encrypt password
```
    #Encrypt password menggunakan base64
        encrypted_password(){
        echo -n "$1" | base64
        }

        encrypted_pass=$(encrypted_password "$password")
```
Fungsi ini digunakan untuk mengenkripsi kata sandi menggunakan algoritma Base64. Fungsi menerima kata sandi sebagai input, lalu mengenkripsinya menggunakan perintah Base64. Hasil enkripsi ini akan disimpan dalam variabel "encrypted_pass".

```
#Simpan data register ke dalam filename
    	echo "$email:$username:$encrypted_pass" >> "$filename"
echo "REGISTER SUCCESS - Registration Success"

#Simpan log ke auth.log
    note_log "REGISTER SUCCESS " "User $username registered successfully"
    }
register
```
Data registrasi pengguna yang telah diinputkan (email, username, dan password yang terenkripsi) disimpan ke dalam file "users.txt".  Kemudian, pesan sukses registrasi akun akan ditampilkan pada layar dan informasi tersebut dicatat pada file log auth.log

Setelah pembuatan skrip register.sh telah selesai, dibutuhkan izin untuk menjalankan skrip "register.sh" dengan menggunakan perintah chmod
```
chmod u+x register.sh
```
Untuk menjalankan program register.sh (Program untuk melakukan register) dapat menjalankan dengan mengetikkan perintah berikut pada terminal 
```
./register.sh
```
Kemudian akan diminta untuk menuliskan email, username dan password yang diinginkan
![Screenshot_2023-09-29_100510](/uploads/252d02e3ce188d2f2db40b4a0d51ebe7/Screenshot_2023-09-29_100510.png)

Apabila Email, username dan password sesuai dengan yang dicantumkan pada soal maka hasilnya dapat dilihat di dalam file users.txt dan log.txt

Hasil pada users.txt
![Screenshot_2023-09-29_100953](/uploads/0a4de8fb2f7f8f3d855e27b56dc40925/Screenshot_2023-09-29_100953.png)

Hasil pada log.txt
![Screenshot_2023-09-29_101036](/uploads/110a68dcb84a92d4523ab4e258f253bd/Screenshot_2023-09-29_101036.png)


## Sistem Login
Berikutnya yaitu membuat sistem login, script tersebut digunakan untuk login, dengan menggunakan email dan password yang telah ada di users.txt atau yang sebelumnya telah didaftarkan menggunakan script register.sh.

Berikut ini adalah penjelasan untuk login.sh:

Untuk membuat program register.sh, dapat membuat bash dengan mengetikkan perintah berikut pada terminal 

```bash
nano login.sh
```

```bash
#!/bin/bash

filename="users.txt"
auth_file="auth.log"
note_log() {
        local type="$1"
        local messages="$2"
        local timestamp=$(date "+[%d/%m/%y %H:%M:%S]")
        echo "$timestamp [$type] $message" >> "$auth_file"
        }
```
Sama seperti pada register.sh, terdapat deklarasi variabel yang meliputi filename mendeklarasikan variable users.txt yang berisi nama file untuk menyimpan informasi pengguna, dan juga ada auth_file mendeklarasikan auth.log yang berisi nama file log yang di autentikasi.

```bash
    read -p "Enter your email: " email
    read -s -p "Enter your password: " password
    echo

```
Pengguna diminta untuk menginput alamat email dengan menggunakan perintah read. Prompt "Enter your Email:" akan ditampilkan, dan input yang dimasukkan akan disimpan dalam variabel email. Selanjutnya, pengguna menginput password, pada proses read password ditambah -s agar password yang diinput pengguna tidak tampil. Lalu input yang dimasukkan akan disimpan dalam variabel password.

```bash
    if grep -q "^$email:" "$filename"; then
    username=$(grep "^$email:" "$filename" | cut -d ':' -f 2)
    password_stored=$(grep "^$email:" "$filename" | cut -d ':' -f 3)
```

Setelah alamat email dan password di input, program akan memeriksa email yang dimasukkan pengguna ada dalam file "users.txt" dengan menggunakan perintah `grep`. Kode `^$email:` digunakan untuk mencocokkan baris yang dimulai dengan alamat email yang dimasukkan oleh pengguna. Jika email tersebut ditemukan dalam file, maka blok ini akan dieksekusi.

```bash
    encrypted_password=$(echo -n "$password_stored" | base64 -d)
    if [[ "$password" == "$encrypted_password" ]]; then
 		    echo "LOGIN SUCCESS - Welcome, $username"
            note_log "LOGIN SUCCESS" "User $username logged in successfully"
    else
            echo "LOGIN FAILED - Failed login attempt"
            note_log "LOGIN FAILED" "ERROR Failed login attempt on user with email $email"
        fi
    else
            echo "LOGIN FAILED - Email $email not registered or incorrect password"
            note_log "LOGIN FAILED" "ERROR Failed login attempt on user with email $email"
    		fi
}
login
```
Password yang tersimpan dalam file "users.txt" merupakan password yang telah terenkripsi, perintah `base64 -d` digunakan untuk mendekripsi password yang dienkripsi dan hasilnya disimpan dalam variabel `encrypted_password`.

Program membandingkan kata sandi yang dimasukkan oleh pengguna dengan kata sandi yang telah dideskripsi. Jika keduanya cocok, maka login dianggap berhasil dan pesan "LOGIN SUCCESS - Welcome, $username" dicetak ke layar dan informasi tersebut dicatat pada file log auth.log dengan menggunakan fungsi note_log. 

Sedangkan,  jika alamat email tidak ditemukan dalam file atau kata sandi yang dimasukkan salah, maka bagian `else` akan dieksekusi. Pesan yang sesuai dengan kegagalan login akan dicetak ke layar, dan informasi tersebut dicatat pada file log auth.log dengan menggunakan fungsi note_log. 

Setelah pembuatan skrip login.sh telah selesai, dibutuhkan izin untuk menjalankan skrip "login.sh" dengan menggunakan perintah chmod

```bash
chmod u+x login.sh
```
Untuk menjalankan program login.sh apat menjalankan dengan mengetikkan perintah berikut pada terminal 

```bash
./login.sh
```

Kemudian akan diminta untuk menuliskan email, dan password yang telah di register sebelumnya. Jika login gagal akan muncul pesan LOGIN FAILED dan jika sukses akan muncul pesan LOGIN SUCCESS:
![Screenshot_2023-09-29_102212](/uploads/10c40a6973935ede173c875bfb181adb/Screenshot_2023-09-29_102212.png)

Hasil pada log.txt:
![Screenshot_2023-09-28_143709](/uploads/4b5ca31b539a5fb50dfdbe050c7bebdd/Screenshot_2023-09-28_143709.png)


## Soal 3
The primary objective of this question is to find a hidden image within image files downloaded from the provided zip file. 

### Downloading the zip file
Only take the file ID when downloading files from Google Drive.
```
wget -O genshin.zip --no-check-certificate -r 'https://drive.google.com/uc?export=download&id1RBuZBtK6q-_I7S_WgeioqIRrUlvRRbq2'
```
| Flag | Description |
| ---------------------- | ---------- |
| -O | writes document to file |
| --no-check-certificate | don't validate the server's certificate |
| -r | recursive |

### Create genshin.sh script
```
code genshin.sh
```

### Inside genshin.sh
**- Code (Part 1)**

In part 1, file names are to be decoded.

```
#!/bin/bash

# revised
mkdir genshin_character 

unzip genshin_character.zip -d genshin_character

gc="genshin_character"
list="list_character.csv"

for file in "$gc"/*; do
	if [ -f "$file" ]; then
		filename=$(basename -- "$file" | awk -F'.' '{print $1}')

		decoded=$(echo -n "$filename" | base64 -d)
		mv "$file" "$gc/$decoded.jpg"
	fi
done
```

**- Explanation of code (Part 1)**

Inlcude shebang as bash interpreter.
```
#!/bin/bash
```

(Revised) Create a new directory to store the the contents of the downloaded zip file.
```
mkdir genshin_character
```

Unzip the zip file that has been downloaded. 
-d is used to place the unzipped file in the destination directory, in this case the "genshin_character" directory.
```
unzip genshin_character.zip -d genshin_character
```

Declare variables for "genshin_character" directory and "list_character.csv" for cleaner code.
```
gc="genshin_character"
list="list_character.csv"
```

Iterate through the whole "genshin_character" directory. 
This is done by looping through all the files within the "genshin_character" directory using the wildcard ("$gc"/*) 
```
for file in "$gc"/*; do
```

Check if it's a file to ensure that only files are decoded. -f ensures that it's only taking a regular file and not a directory or anything else.
	
(Alternative: Given that the question specifically requests to decode image files, it is also acceptable to use a wildcard approach that selects all files in the jpg format using the command "$gc"/*.jpg. This approach also guarantees that all selected files are indeed images.)
```
if [ -f "$file" ]; then
```

Declare a variable called "filename" which purpose is to store the file name without the path of the file, then takes the basename of the file, and removes its extension. 

-F'.' means awk uses the dot as a field separator, in this case it separates the file name as the first field and the extension "jpg" as the second field. {print $1} then prints the first field which is the file name.
```
filename=$(basename -- "$file" | awk -F'.' '{print $1}')
```

Declare a variable called "decoded" which purpose is to store the decoded value of the previously file name. 
base64 -d is used to decode.
```
decoded=$(echo -n "$filename" | base64 -d)
```

Rename selected files with the decoded file names by using the mv command. 
Here the "file" variable is the old file name and the "decoded" variable is the new file names.
```
        mv "$file" "$gc/$decoded.jpg"
    fi
done
```
The genshin_character directory now has files renamed according to its decoded names, as shown below:
![Screenshot_2023-09-29_112407](/uploads/5ca946284d2c37121984327c44092434/Screenshot_2023-09-29_112407.png)

**- Code (Part 2)**

In part 2, the file names are now renamed according to its desired format, according to the csv list.
```
dos2unix character_list.csv #revised

while IFS=',' read -r name region element weapon; do
	name=$(echo "$name" | awk -F ', *' '{print $1}')

	old="$gc/$name.jpg"	
	new="$gc/$name - $region - $element - $weapon.jpg"
	mv "$old" "$new"
done < $list
```

**- Explanation of code (Part 2)**


One challenge encountered when working on this question is the new lines that shows up when renaming files. To fix this, the character_list.csv file is converted to unix format.


(Revised) dos2unix used to remove new lines.
```
dos2unix character_list.csv #revised
```

Read each line that were seperated by a comma and assign variables to each of them according to the category, in this case it's name, region, element, and weapon.

IFS here uses "," as its internal field separator.
```
while IFS=',' read -r name region element weapon; do
```

Ensure that white spaces are also included when reading names.

-F', *' means awk uses the comma as a field separator, in this case it also uses the wildcard to include white spaces. {print $1} then prints the first field which is the file name.
    
```
    name=$(echo "$name" | awk -F ', *' '{print $1}')
```

Rename the files according to the desired format.
```
    old="$gc/$name.jpg" 
    new="$gc/$name - $region - $element - $weapon.jpg"
```

Rename selected files with the decoded file names by using the mv command. 
```
    mv "$old" "$new"
```

Read lines from character_list.csv
```
done < $list
```

The genshin_character directory now has files renamed according to its desired format, according to the character_list.csv file, as shown below:
![Screenshot_2023-09-29_112208](/uploads/622dfdc6ab136a1123582fa7865c2278/Screenshot_2023-09-29_112208.png)

**- Code (Part 3)**

In part 3, the script counts each weapon.
```
declare -A weapon_counter

while IFS="," read -r name region element weapon; do
	if [[ -n ${weapon_counter["$weapon"]} ]]; then
		((weapon_counter["$weapon"]++))
	else
		weapon_counter["$weapon"]=1
	fi	
done <$list

for weapon in "${!weapon_counter[@]}"; do
	count="${weapon_counter["$weapon"]}"
	echo "$weapon : $count"
done
```

**- Explanation of code (Part 3)**

Declare an associative array to keep track of how many weapon appears in the list.
```
declare -A weapon_counter
```

Take weapon from list.
```
while IFS="," read -r name region element weapon; do
```
Check if weapon is already in array.
```
    if [[ -n ${weapon_counter["$weapon"]} ]]; then
```

Add count if it is already in array.
```
        ((weapon_counter["$weapon"]++))
```
Add weapon to array if weapon is not in array.
```
    else
        weapon_counter["$weapon"]=1
    fi
```

Read lines from character_list.csv.
```
done <$list
```

Print the associative array.
```
for weapon in "${!weapon_counter[@]}"; do
    count="${weapon_counter["$weapon"]}"
    echo "$weapon : $count"
done
```

Delete unnecessary files.
```
rm genshin_character.zip
rm list_character.csv
rm genshin.zip
```


The number of weapons will be echoed, as shown here:
![Screenshot_2023-09-29_105207](/uploads/7f40ae20968291645aa5dd559348005c/Screenshot_2023-09-29_105207.png)


### Create find_me.sh script

```
code find_me.sh
```

### Inside find_me.sh

**- Code**


This script extracts image files every one second and saves its log in image.log.

```
#!/bin/bash
gc=genshin_character
for file in "$gc"/*; do
    if [ -f "$file" ];then
        timestamp=$(date "+[%d/%m/%y %H:%M:%S]")
        image_path=$(realpath "$file")
        steghide extract -sf "$file" -p "" -f
        sleep 1
        found=$(cat *.txt | base64 -d 2>/dev/null | grep -i "https")
        
        if echo $found | grep -i "https"; then
            wget "$found"
            echo "$timestamp [FOUND] [$image_path]" >> image.log
            exit
        else
            echo "$timestamp [NOT FOUND] [$image_path]" >> image.log
            rm *.txt
        fi
    fi
done
```

**- Explanation**


Include shebang as bash interpreter and declare variables for cleaner code.
```
#!/bin/bash
gc=genshin_character
```

Similar to the first part of genshin.sh code, iterate through the whole genshin_character directory and ensure that  it's a file.
```
for file in "$gc"/*; do
    if [ -f "$file" ];then
```

Create variables for timestamp and image path. This will be used when writing logs.
```
        timestamp=$(date "+[%d/%m/%y %H:%M:%S]")
        image_path=$(realpath "$file")
```

Extract each image using steghide, -sf is used to specify the source file, while -p is to indicate the passphrase which in this case. there is no passphrase required.

Sleep 1 is then used to pause the extraction by 1 second within each loop.
```
        steghide extract -sf "$file" -p "" -f
        sleep 1
```

To look for the hidden image link, inspect the content of the txt file that has been extracted previously, then decode the content. To redirect error messages, ```2>/dev/null``` is used. To look for the link, grep is used with -i flag to perform a case insensitive search.
```
        found=$(cat *.txt | base64 -d 2>/dev/null | grep -i "https")
```

Download the image file when 'https' is found in the decoded text, then record the timestamp and image_path as FOUND in image.log, and stop the program. Otherwise, record timestamp and image-path as NOT FOUND, and delete the extracted txt file.
```
        if echo $found | grep -i "https"; then
            wget "$found"
            echo "$timestamp [FOUND] [$image_path]" >> image.log
            exit
        else
            echo "$timestamp [NOT FOUND] [$image_path]" >> image.log
            rm *.txt
        fi
    fi
done
```

The image.log file will record logs, as shown below:
![Screenshot_2023-09-29_154315](/uploads/a81ecc60e4550d5beb708f931aabedf5/Screenshot_2023-09-29_154315.png)

### Hidden image
![eab0659ba060b0624df5d28b4910b2fb](/uploads/1e5a10193543a52f0b521c5d48ecec84/eab0659ba060b0624df5d28b4910b2fb.jpg)

## Soal 4
Berdasarkan soal, terdapat dua file script dan log.  

### Code minute_log.sh
```
#!/bin/bash

timestamp=$(date "+%Y%m%d%H%M%S")

ram_metrics=$(free -m)

target_path="/home/stephanie/soal_4/log/"
disk_metrics=$(du -sh "$target_path")

log_file="/home/stephanie/soal_4/log/metrics_$timestamp.log"

echo "$ram_metrics,$target_path,$disk_metrics" > "$log_file"

# * * * * * /home/stephanie/soal_4/script/minute_log.sh

```

```timestamp=$(date "+%Y%m%d%H%M%S")``` digunakan untuk menandakan/menetapkan tanggal dan waktu sekarang ke dalam format tertentu. ```+%Y%m%d%H%M%S``` digunakan untuk penentuan format tahun (4 digit, contoh = 2023), penentuan bulan (2 digit, 09 = September), penentuan hari (2 digit, 28), penentuan jam dalam format 24 jam (14 untuk pukul 2 sore), penentuan menit (2 digit), dan detik (2 digit).

```ram_metrics=$(free -m)``` digunakan untuk mengambil dan menampilkan info tentang penggunaan RAM

```
target_path="/home/stephanie/soal_4/log/"

disk_metrics=$(du -sh "$target_path")
```

Target_path sebagai direktori yang ingin diperiksa yaitu file log. Disk_metrics adalah jumlah total penggunaan disk oleh direktori yang ada di target_path, du adalh disk usage untuk menghitung ukuran penggunaan disk, -s digunakan untuk menyajikan ringkasan yang manampilkan ukuran total penggunaan disk, -h digunakan untuk membuat laporan ukuran disk. 

```log_file="/home/stephanie/soal_4/log/metrics_$timestamp.log"``` ini berarti log file akan keluar di direktori menuju log, dan akan keluar output metrics_(waktu sekarang).log

```echo "$ram_metrics,$target_path,$disk_metrics" > "$log_file"``` echo digunakan untuk menampilkan teks/variable. ```$ram_metrics,$target_path,$disk_metrics``` adalah string yang berisi pengunaan RAM, berisi path yang ingin diperiksa, dan info disk. ```> "$log_file"``` adalah output yang mengarah ke file di log file

/# * * * * * /home/stephanie/soal_4/script/minute_log.sh digunakan untuk soal 4b, dimana Cron jobs adalah sebuah service daemon yang memungkinkan user Linux dan Unix untuk menjalankan perintah atau script pada waktu tertentu secara otomatis. * * * * *  berarti waktunya otomatis “at every minute” 

**Output di log**
![Screenshot_2023-09-29_192242](/uploads/7a12aa708bd2bf182e87b9cf77a46bcb/Screenshot_2023-09-29_192242.png)

**Cat log**
![Screenshot_2023-09-29_192355](/uploads/631e776adc9688225eb9bc3d07497432/Screenshot_2023-09-29_192355.png)

### Revised code for minute_log.sh

Create log directory before executing the minute_log.sh script
```
mkdir log
```
minute_log.sh script:
```
#!/bin/bash
# crontab comment as requested in the question
# * * * * * /mnt/d/SISOP/sisop/modul-1/soal_4/minute_log.sh

timestamp=$(date "+%Y%m%d%H%M%S")
log_file="metrics_$timestamp.log"

# ram (revised)
ram_metrics=$(free -m | grep -i mem | awk '{print $2","$3","$4","$5","$6","$7}')

# swap (revised)
swap_metrics=$(free -m | grep -i swap | awk '{print $2","$3","$4}')

# disk (revised)
target_path="/home/suguru"
disk_metrics=$(du -sh "$target_path" | awk '{print $1}')

# revised
echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> "/mnt/d/SISOP/sisop/modul-1/soal_4/log/$log_file"
echo "$ram_metrics,$swap_metrics,$target_path,$disk_metrics" >> "/mnt/d/SISOP/sisop/modul-1/soal_4/log/$log_file"

chmod 700 "/mnt/d/SISOP/sisop/modul-1/soal_4/$log_file"
```

In this revised version, the contents of the log file is now in the desired format. 


**Explanation of revised minute_log.sh**


```
ram_metrics=$(free -m | grep -i mem | awk '{print $2","$3","$4","$5","$7}')
```
```grep -i mem``` is used to take the Mem row. awk is then used to take the values that needs to be taken.

Details of Mem row:
- $2 refers to total memory
- $3 refers to used memory
- $4 refers to free memory
- $5 refers to shared memory
- $6 refers to buff/cache memory
- $7 refers to available memory

The swap metrics and disk metrics follows a similar method.


Details of Swap row:
- $2 refers to total swap
- $3 refers to used swap
- $4 refers to free swap


```
echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> "/mnt/d/SISOP/sisop/modul-1/soal_4/log/$log_file"
echo "$ram_metrics,$swap_metrics,$target_path,$disk_metrics" >> "/mnt/d/SISOP/sisop/modul-1/soal_4/log/$log_file"
```

To limit accessibility, chmod 700 is used and sets permissions so that, (U)ser / owner can read, can write and can execute. (G)roup can't read, can't write and can't execute. (O)thers can't read, can't write and can't execute.
```
chmod 700 "/mnt/d/SISOP/sisop/modul-1/soal_4/$log_file"
```
Reference: https://chmodcommand.com/chmod-700/

Reference: https://crontab.guru/#*_*_*_*_*



Each information is then stored in log_file, according to the format specified in the question, as shown below:
![Screenshot_2023-09-29_231951](/uploads/90c54a438bcdad7e8abfbddbe1dc002b/Screenshot_2023-09-29_231951.png)


### Revised code for aggregate_minutes_to_hourly_log.sh

```
#!/bin/bash
# crontab comment as requested in the question
# 0 * * * * /mnt/d/SISOP/sisop/modul-1/soal_4/aggregate_minutes_to_hourly_log.sh


function minutelogs(){
    for file in $(ls log/metrics_* | grep -v agg | grep $(date +"%Y%m%d%H")); do 
        cat $file | grep -v mem
    done
}

mem_total_find=$(minutelogs | awk -F, '{
    if(min=="") {min=max=$1}; 
    if($1>max) {max=$1};
    if($1<min) {min=$1};
    total+=$1; count+=1
} END {
    avg=total/count
    print min,max,avg
}')

mem_total_min=$(echo $mem_total_find | awk '{print $1}' | tr ',' '.')
mem_total_max=$(echo $mem_total_find | awk '{print $2}' | tr ',' '.')
mem_total_avg=$(echo $mem_total_find | awk '{print $3}' | tr ',' '.')

mem_used_find=$(minutelogs | awk -F, '{
    if(min=="") {min=max=$2}; 
    if($2>max) {max=$2};
    if($2<min) {min=$2};
    total+=$2; count+=1
} END {
    avg=total/count
    print min,max,avg
}')

mem_used_min=$(echo $mem_used_find | awk '{print $1}' | tr ',' '.')
mem_used_max=$(echo $mem_used_find | awk '{print $2}' | tr ',' '.')
mem_used_avg=$(echo $mem_used_find | awk '{print $3}' | tr ',' '.')

mem_free_find=$(minutelogs | awk -F, '{
    if(min=="") {min=max=$3}; 
    if($3>max) {max=$3};
    if($3<min) {min=$3};
    total+=$3; count+=1
} END {
    avg=total/count
    print min,max,avg
}')

mem_free_min=$(echo $mem_free_find | awk '{print $1}' | tr ',' '.')
mem_free_max=$(echo $mem_free_find | awk '{print $2}' | tr ',' '.')
mem_free_avg=$(echo $mem_free_find | awk '{print $3}' | tr ',' '.')

mem_shared_find=$(minutelogs | awk -F, '{
    if(min=="") {min=max=$4}; 
    if($4>max) {max=$4};
    if($4<min) {min=$4};
    total+=$4; count+=1
} END {
    avg=total/count
    print min,max,avg
}')

mem_shared_min=$(echo $mem_shared_find | awk '{print $1}' | tr ',' '.')
mem_shared_max=$(echo $mem_shared_find | awk '{print $2}' | tr ',' '.')
mem_shared_avg=$(echo $mem_shared_find | awk '{print $3}' | tr ',' '.')

mem_buff_find=$(minutelogs | awk -F, '{
    if(min=="") {min=max=$5}; 
    if($5>max) {max=$5};
    if($5<min) {min=$5};
    total+=$5; count+=1
} END {
    avg=total/count
    print min,max,avg
}')

mem_buff_min=$(echo $mem_buff_find | awk '{print $1}' | tr ',' '.')
mem_buff_max=$(echo $mem_buff_find | awk '{print $2}' | tr ',' '.')
mem_buff_avg=$(echo $mem_buff_find | awk '{print $3}' | tr ',' '.')

mem_available_find=$(minutelogs | awk -F, '{
    if(min=="") {min=max=$6}; 
    if($6>max) {max=$6};
    if($6<min) {min=$6};
    total+=$6; count+=1
} END {
    avg=total/count
    print min,max,avg
}')

mem_available_min=$(echo $mem_available_find | awk '{print $1}' | tr ',' '.')
mem_available_max=$(echo $mem_available_find | awk '{print $2}' | tr ',' '.')
mem_available_avg=$(echo $mem_available_find | awk '{print $3}' | tr ',' '.')

swap_total_find=$(minutelogs | awk -F, '{
    if(min=="") {min=max=$7}; 
    if($7>max) {max=$7};
    if($7<min) {min=$7};
    total+=$7; count+=1
} END {
    avg=total/count
    print min,max,avg
}')

swap_total_min=$(echo $swap_total_find | awk '{print $1}' | tr ',' '.')
swap_total_max=$(echo $swap_total_find | awk '{print $2}' | tr ',' '.')
swap_total_avg=$(echo $swap_total_find | awk '{print $3}' | tr ',' '.')

swap_used_find=$(minutelogs | awk -F, '{
    if(min=="") {min=max=$8}; 
    if($8>max) {max=$8};
    if($8<min) {min=$8};
    total+=$8; count+=1
} END {
    avg=total/count
    print min,max,avg
}')

swap_used_min=$(echo $swap_used_find | awk '{print $1}' | tr ',' '.')
swap_used_max=$(echo $swap_used_find | awk '{print $2}' | tr ',' '.')
swap_used_avg=$(echo $swap_used_find | awk '{print $3}' | tr ',' '.')

swap_free_find=$(minutelogs | awk -F, '{
    if(min=="") {min=max=$9}; 
    if($9>max) {max=$9};
    if($9<min) {min=$9};
    total+=$9; count+=1
} END {
    avg=total/count
    print min,max,avg
}')

swap_free_min=$(echo $swap_free_find | awk '{print $1}' | tr ',' '.')
swap_free_max=$(echo $swap_free_find | awk '{print $2}' | tr ',' '.')
swap_free_avg=$(echo $swap_free_find | awk '{print $3}' | tr ',' '.')

path_size_find=$(minutelogs | awk -F, '{
    if(min=="") {min=max=$11}; 
    if($11>max) {max=$11};
    if($11<min) {min=$11};
    total+=$11; count+=1
} END {
    avg=total/count
    print min,max,avg
}')

path_size_min=$(echo $path_size_find | awk '{print $1}' | tr ',' '.')
path_size_max=$(echo $path_size_find | awk '{print $2}' | tr ',' '.')
path_size_avg=$(echo $path_size_find | awk '{print $3}' | tr ',' '.')

target_path="/home/suguru" 
hour_timestamp=$(date +"%Y%m%d%H")
agg_logfile="metrics_agg_$hour_timestamp.log"

echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> "/mnt/d/SISOP/sisop/modul-1/soal_4/log/$agg_logfile" 

echo "minimum,$mem_total_min,$mem_used_min,$mem_free_min,$mem_shared_min,$mem_buff_min,$mem_available_min,$swap_total_min,$swap_used_min,$swap_free_min,$target_path,$path_size_min" >> "/mnt/d/SISOP/sisop/modul-1/soal_4/log/$agg_logfile" 
echo "maximum,$mem_total_max,$mem_used_max,$mem_free_max,$mem_shared_max,$mem_buff_max,$mem_available_max,$swap_total_max,$swap_used_max,$swap_free_max,$target_path,$path_size_max" >> "/mnt/d/SISOP/sisop/modul-1/soal_4/log/$agg_logfile" 
echo "average,$mem_total_avg,$mem_used_avg,$mem_free_avg,$mem_shared_avg,$mem_buff_avg,$mem_available_avg,$swap_total_avg,$swap_used_avg,$swap_free_avg,$target_path,$path_size_avg" >> "/mnt/d/SISOP/sisop/modul-1/soal_4/log/$agg_logfile" 

chmod 700 "/mnt/d/SISOP/sisop/modul-1/soal_4/log/$agg_logfile" 
```

### Explanation of revised aggregate_minutes_to_hourly_log.sh
```
function minutelogs(){
    for file in $(ls log/metrics_* | grep -v agg | grep $(date +"%Y%m%d%H")); do 
        cat $file | grep -v mem
    done
}
```
1. This function iterates through the log directory to take files which starts with "metrics_". This is done by utilizing the wildcard. 
2. ```grep -v agg``` ensures that no aggregate metrics are taken. 
3. ```grep $(date +"%Y%m%d%H"``` takes "metrics_*" files that were made in the specific hour. 
4. The metrics content of each file is then taken by leaving out "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size". This is done by using ```grep -v mem``` which excludes this whole line 


```
mem_total_find=$(minutelogs | awk -F, '{
    if(min=="") {min=max=$1}; 
    if($1>max) {max=$1};
    if($1<min) {min=$1};
    total+=$1; count+=1
} END {
    avg=total/count
    print min,max,avg
}')
```

To find the minimum, maximum, and average of the total memory metrics that were previously taken, the mem_total_find variable first looks through the files, and then uses ```awk -F,``` to take the value separated by a comma.

**How mem_total_find works:**

1. ```if(min=="") {min=max=$1}; ```: Initialized min and max variables and take value from $1 which refers to total memory
2. ```if($1>max) {max=$1};```: Update max value
3. ```if($1<min) {min=$1};```: Update min value
4. ```total+=$1; count+=1```: Keep track of the sum of total memory and the amount of metrics logs that exists
5. ```avg=total/count```: Calculate average by dividing the sum of total memory by the amount of metrics logs
6. ```print min,max,avg```: Print minimum, maximum, and average value separated by comma to utilize awk later on.


```
mem_total_min=$(echo $mem_total_find | awk '{print $1}' | tr ',' '.')
mem_total_max=$(echo $mem_total_find | awk '{print $2}' | tr ',' '.')
mem_total_avg=$(echo $mem_total_find | awk '{print $3}' | tr ',' '.')
```
The minimum, maximum, and average value is then stored in their own variable by utilizing awk. Any comma found is also replaced by a dot to prevent confusion when each values are stored in the aggregate metrics logs file. 


The method above is then used to get the values of each metrics, including total memory, used memory, free memory, shared memory, buff/cache memory, available memory, total swap, used swap, free swap, and path size. 

```
target_path="/home/suguru" 
hour_timestamp=$(date +"%Y%m%d%H")
agg_logfile="metrics_agg_$hour_timestamp.log"

```
The above variables are simply used for cleaner code.

```
echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> "/mnt/d/SISOP/sisop/modul-1/soal_4/log/$agg_logfile" 

echo "minimum,$mem_total_min,$mem_used_min,$mem_free_min,$mem_shared_min,$mem_buff_min,$mem_available_min,$swap_total_min,$swap_used_min,$swap_free_min,$target_path,$path_size_min" >> "/mnt/d/SISOP/sisop/modul-1/soal_4/log/$agg_logfile" 
echo "maximum,$mem_total_max,$mem_used_max,$mem_free_max,$mem_shared_max,$mem_buff_max,$mem_available_max,$swap_total_max,$swap_used_max,$swap_free_max,$target_path,$path_size_max" >> "/mnt/d/SISOP/sisop/modul-1/soal_4/log/$agg_logfile" 
echo "average,$mem_total_avg,$mem_used_avg,$mem_free_avg,$mem_shared_avg,$mem_buff_avg,$mem_available_avg,$swap_total_avg,$swap_used_avg,$swap_free_avg,$target_path,$path_size_avg" >> "/mnt/d/SISOP/sisop/modul-1/soal_4/log/$agg_logfile" 

```

Store each values to the aggregate metrics log file, according to its hour. 

```
chmod 700 "/mnt/d/SISOP/sisop/modul-1/soal_4/log/$agg_logfile" 
```
Similar to minute_log.sh, chmod 700 is used to limit permissions.


The metrics_agg_$hour_timestamp.log stores information like below:
![Screenshot_2023-09-30_094315](/uploads/ec95c5c0c8f64ba51e42992fc2c9d3fa/Screenshot_2023-09-30_094315.png)

---
Sekian pengerjaan laporan resmi praktikum SISOP modul 1 dari kami, IT02 signing offfff!!!!!!!! 
