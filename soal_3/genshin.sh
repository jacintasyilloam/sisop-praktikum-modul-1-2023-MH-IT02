#!/bin/bash

unzip genshin_character.zip -d genshin_character

gc="genshin_character"
list="list_character.csv"

for file in "$gc"/*; do
	if [ -f "$file" ]; then
		filename=$(basename -- "$file" | awk -F'.' '{print $1}')

		decoded=$(echo -n "$filename" | base64 -d)
		mv "$file" "$gc/$decoded.jpg"
	fi
done

while IFS=',' read -r name region element weapon; do
	name=$(echo "$name" | awk -F ', *' '{print $1}')

	old="$gc/$name.jpg"	
	new="$gc/$name - $region - $element - $weapon.jpg"
	mv "$old" "$new"
done < $list

declare -A weapon_counter

while IFS="," read -r name region element weapon; do
	if [[ -n ${weapon_counter["$weapon"]} ]]; then
		((weapon_counter["$weapon"]++))
	else
		weapon_counter["$weapon"]=1
	fi
done <$list

for weapon in "${!weapon_counter[@]}"; do
	count="${weapon_counter["$weapon"]}"
	echo "$weapon : $count"
done

#rm genshin_character.zip
#rm list_character.csv
#rm genshin.zip
