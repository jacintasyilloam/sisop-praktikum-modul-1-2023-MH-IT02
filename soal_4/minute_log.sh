#!/bin/bash

timestamp=$(date "+%Y%m%d%H%M%S")

ram_metrics=$(free -m)

target_path="/home/stephanie/soal_4/log/"
disk_metrics=$(du -sh "$target_path")

log_file="/home/stephanie/soal_4/log/metrics_$timestamp.log"

echo "$ram_metrics,$target_path,$disk_metrics" > "$log_file"

# * * * * * /home/stephanie/soal_4/script/minute_log.sh

